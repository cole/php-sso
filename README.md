#php-sso

php-sso是一个简单的实现了部分cas原理的单点登录系统。

#目录结构
phpsso

	单点登录的验证中心。

ssoclient1

	测试客户端1

	--ssosdk

		单点登录的客户端

ssoclient2

	测试客户端2

#如何安装
1、在目录phpsso下有一个mrsso.sql的数据库文件，导库即可。同时修改phpsso目录下的.env文件，里面的数据库配置，大家根据自己的情况来设定。

2、ssoclient1或ssoclient2下的ssosdk/config/config.php配置。主要是域名的配置，如果不同改为自己配置就行了。其中ssoclient的值取值为phpsso目录下的config/sso.php中的services的值所对应的key。所有的客户端需要在这里进行预先配置。

3、配置好后，访问任务一个client，如果没有登录那么会跳转到phpsso验证登录。当登录成功后，访问另一个client则不需要登录了。

#关于测试
ssoclient1和ssoclient2只是两个简单的样例，我个人也没有进行过非常严格的测试。如果有问题，请大家提交issues。

#关于php版本
至少满足laravel框架的要求。

希望大家喜欢！
